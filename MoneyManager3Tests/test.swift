import XCTest
class UpdateCheckTests: XCTestCase {
    var mock: UpdateMock!
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        mock = UpdateMock()
        continueAfterFailure = false
        app.launch()
    }
    func testUpdateCheck() {
        XCTAssertTrue(mock.isUpdateAvailable(),"This is latest version")
    }
    
    func permissionCheck() {
        
    }
    
    func dbchcek() {
        
    }
}

class UpdateMock: Update  {
    func isUpdateAvailable() -> Bool {
        guard let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundldId=com.shinhan.foreignerbank"),
            let data = try? Data(contentsOf: url),
            let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any],
            let results = json["results"] as? [[String: Any]],
            results.count > 0 ,
            let appStoreVersion = results[0]["version"] as? String
            else { return false }
        
        if !(version == appStoreVersion) { return true }
        else { return false }
        
        return false
    }
    
}


protocol Update {
    func isUpdateAvailable() -> Bool
}




