//
//  MoneyManager3Tests.swift
//  MoneyManager3Tests
//
//  Created by Lee Hong Jae on 2019/10/26.
//  Copyright © 2019 Lee Hong Jae. All rights reserved.
//

import XCTest
@testable import MoneyManager3

class MoneyManager3Tests: XCTestCase {

    var mock:Mock!
    let viewController = ViewController()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        mock = Mock()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testVersionCheck() {
        
        var result:Bool!
        
        mock.setVersion(false)
        result = viewController.DoStartCheck(mock.versionCheck(), true, true)
        XCTAssertFalse(result, "version fail check fail")
        
        mock.setVersion(true)
        result = viewController.DoStartCheck(mock.versionCheck(), true, true)
        XCTAssertTrue(result, "version true check fail")
    }
    
    func testPermissionCheck() {
        
        var result:Bool!
        
        mock.setPermission(false)
        result = viewController.DoStartCheck(true, mock.permissionCheck(), true)
        XCTAssertFalse(result, "permission fail check fail")
        
        mock.setPermission(true)
        result = viewController.DoStartCheck(true, mock.permissionCheck() ,true)
        XCTAssertTrue(result, "permission true check fail")
    }
    
    func testDBCheck() {
        
        var result:Bool!
        
        mock.setDB(false)
        result = viewController.DoStartCheck(true, true, mock.DBCheck())
        XCTAssertFalse(result, "DB fail check fail")
        
        mock.setDB(true)
        result = viewController.DoStartCheck(true, true, mock.DBCheck())
        XCTAssertTrue(result, "DB true check fail")
    }
    
    func testDoStart() {
        mock.setVersion(true)
        mock.setPermission(true)
        mock.setDB(true)
        
        let result = viewController.DoStartCheck(mock.versionCheck(), mock.permissionCheck(), mock.DBCheck())
        
        XCTAssertTrue(result, "DoStart All check fail")
    }
    
    internal class Mock:MockingForTest {
        
        var versionReturn = false
        var permissionReturn = false
        var DBReturn = false;
        
        func versionCheck() -> Bool {
            return versionReturn
        }
        
        func permissionCheck() -> Bool {
            return permissionReturn
        }
        
        func DBCheck() -> Bool {
            return DBReturn
        }
        
        func setVersion(_ checkResult:Bool) {
            self.versionReturn = checkResult
        }
        
        func setPermission(_ checkResult:Bool) {
            self.permissionReturn = checkResult
        }
        
        func setDB(_ checkResult:Bool) {
            self.DBReturn = checkResult
        }
    }
}
