//
//  MockingForTest.swift
//  MoneyManager3
//
//  Created by Lee Hong Jae on 2019/10/26.
//  Copyright © 2019 Lee Hong Jae. All rights reserved.
//

import Foundation


protocol MockingForTest {
    func versionCheck() -> Bool
    func permissionCheck() -> Bool
    func DBCheck() -> Bool
}
