//
//  ViewController.swift
//  MoneyManager3
//
//  Created by Lee Hong Jae on 2019/10/26.
//  Copyright © 2019 Lee Hong Jae. All rights reserved.
//

import UIKit
import Contacts
import SQLite3

class ViewController: UIViewController, MockingForTest {

    // 주소록 저장
    var contacts: NSMutableArray = NSMutableArray()
    
    // DB
    var dbPath : String = ""
    let dbName : String = "moneymanager.db"
    let sqlCreate : String = "CREATE TABLE IF NOT EXISTS TEST ( "
                                    + " SEQ     INTEGER PRIMARY KEY AUTOINCREMENT, "
                                    + " DATE    TEXT, "
                                    + " CNT     INTEGER, "
                                    + " NAME    TEXT, "
                                    + " PHONE   TEXT, "
                                    + " AMT     INTEGER"
                                + ")"

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let isOkVersionCheck:Bool = versionCheck()
        let isOkPermissionCheck:Bool = permissionCheck()
        let isOkDBCheck:Bool = DBCheck()
        
        if(DoStartCheck(isOkVersionCheck, isOkPermissionCheck, isOkDBCheck)) {
            print("go main page");
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [unowned self] in
                       self.performSegue(withIdentifier: "MainView", sender: self)
            }
        }
    }
    
    func DoStartCheck(_ isOkVersionCheck:Bool, _ isOkPermissionCheck:Bool, _ isOkDBCheck:Bool) -> Bool {
        
        var msg:String = "[App 기동 오류]"
        
        if(!isOkVersionCheck) {
            msg = msg + "\nVersion Check Fail"
        }
        
        if(!isOkPermissionCheck) {
            msg = msg + "\nPermission Check Fail"
        }
        
        if(!isOkDBCheck) {
            msg = msg + "\nDB Check Fail"
        }
        
        if(!isOkVersionCheck || !isOkPermissionCheck || !isOkDBCheck) {
            let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
            
            return false
        } else {
            return true
        }
    }

    // 최신버전 체크
    func versionCheck() -> Bool {
        print("versionCheck")
        
        guard let MyVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
            // App store version과 비교
              let url = URL(string: "http://itunes.apple.com/lookup?bundleId=com.shinhan.foreignerbank"),

              let data = try? Data(contentsOf: url),
              let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any],
              let results = json["results"] as? [[String: Any]],
              results.count > 0,
              let appStoreVersion = results[0]["version"] as? String
        else { return false }
        
        print("MyVersion",MyVersion)
        print("appStoreVersion",appStoreVersion)
        
        
        if (MyVersion == appStoreVersion) { return true }
        else{ return false }
    }
    
    // 권한 체크
    func permissionCheck() -> Bool {
        print("permissionCheck")
        
        let store = CNContactStore()
        var contactAccess = false
        
        // Permission 획득
        store.requestAccess(for: .contacts) { (granted, error) in
           
            print("granted:",granted)
            contactAccess = granted
            //권한 허용 시
            if(granted) {
                // Request 생성
                let request: CNContactFetchRequest = self.getCNContactFetchRequest()

                // 주소록 읽을 때 정렬해서 읽어오도록 설정
                request.sortOrder = CNContactSortOrder.userDefault

                // Contacts 읽기
                // 주소록이 1개씩 읽혀서 usingBlock으로 들어온다.
                try! store.enumerateContacts(with: request, usingBlock: { (contact, stop) in

                    // Phone No가 없을때 return
                    if contact.phoneNumbers.isEmpty {
                        return
                    }

                    // NSMutableArray Add contact
                    // 읽어온 주소록을 NSMutableArray에 저장
                    self.contacts.add(contact)
                })
            } else {
                return
            }
        }
        print("contactAccess:",contactAccess)
        return contactAccess
    }
    
    // DB 체크
    func DBCheck() -> Bool {
        print("DBCheck")
        
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        let docPath = dirPath[0]
                
        dbPath = docPath+"/"+dbName
        
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: dbPath as String) {
            print("DB fileExists false")
            //DB 객체 생성
            let database : FMDatabase? = FMDatabase(path: dbPath as String)
            
            if let db = database {
                
                //DB 열기
                db.open()
                
                //TABLE 생성
                db.executeStatements(sqlCreate)
                
                //DB 닫기
                db.close()
                
                print("TABLE 생성 성공")
            }else{
                print("DB 객체 생성 실패")
                return false
            }
        } else {
            print("DB fileExists true")
        }
        return true
    }
    
    // Request 생성
    private func getCNContactFetchRequest() -> CNContactFetchRequest {

        // 주소록에서 읽어올 key 설정
        let keys: [CNKeyDescriptor] = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                                       CNContactPhoneNumbersKey] as! [CNKeyDescriptor]

        return CNContactFetchRequest(keysToFetch: keys)
    }
}

